#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/list.hpp>
#include <boost/python/str.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/exception_translator.hpp>
#include <boost/python/class.hpp>
#include <string>
#include <regex>
#include <exception>

struct regex_exception : std::exception
{
    regex_exception() = default;
    regex_exception(const std::string& message) : message(message) {}
    std::string get_message() const { return message; }
    std::string message;
};

PyObject* Regex_Exception_Type = nullptr;

void regex_exception_translator(regex_exception const& e)
{
    assert(Regex_Exception_Type != nullptr);
    boost::python::object pythonExceptionInstance(e);
    PyErr_SetObject(Regex_Exception_Type, pythonExceptionInstance.ptr());
}

void regex_error_translator(const std::regex_error& e) throw(regex_exception)
{
    switch (e.code())
    {
        case std::regex_constants::error_collate:
            throw regex_exception("the expression contains an invalid collating element name");
            break;
        case std::regex_constants::error_ctype:
            throw regex_exception("the expression contains an invalid character class name");
            break;
        case std::regex_constants::error_escape:
            throw regex_exception("the expression contains an invalid escaped character or a trailing escape");
            break;
        case std::regex_constants::error_backref:
            throw regex_exception("the expression contains an invalid back reference");
            break;
        case std::regex_constants::error_brack:
            throw regex_exception("the expression contains mismatched square brackets ('[' and ']')");
            break;
        case std::regex_constants::error_paren:
            throw regex_exception("the expression contains mismatched parentheses ('(' and ')')");
            break;
        case std::regex_constants::error_brace:
            throw regex_exception("the expression contains mismatched curly braces ('{' and '}')");
            break;
        case std::regex_constants::error_badbrace:
            throw regex_exception("the expression contains an invalid range in a {} expression");
            break;
        case std::regex_constants::error_range:
            throw regex_exception("the expression contains an invalid character range (e.g. [b-a])");
            break;
        case std::regex_constants::error_space:
            throw regex_exception("there was not enough memory to convert the expression into a finite state machine");
            break;
        case std::regex_constants::error_badrepeat:
            throw regex_exception("one of *?+{ was not preceded by a valid regular expression");
            break;
        case std::regex_constants::error_complexity:
            throw regex_exception("the complexity of an attempted match exceeded a predefined level");
            break;
        case std::regex_constants::error_stack:
            throw regex_exception("there was not enough memory to perform a match");
            break;
        default:
            throw regex_exception("unknown C++ std::regex_error caught");
            break;
    }
}

boost::python::list match(boost::python::str target_str, boost::python::str regex_str)
{
    const std::string regex_string = boost::python::extract<std::string>(regex_str);
    const std::string target_string = boost::python::extract<std::string>(target_str);
    try {
        std::smatch matches;
        if (std::regex_match(target_string, matches, std::regex(regex_string))) 
        {
            boost::python::list output;
            for (const std::ssub_match& sub_match : matches)
            {
                output.append(boost::python::str(sub_match.str()));
            }
            return output;
        }
    }
    catch (const std::regex_error& e) 
    {
        regex_error_translator(e);
    }

   return boost::python::list();
}

void setup_exceptions()
{
    using namespace boost::python;

    class_<regex_exception> my_exception("regex_exception", init<std::string>());
    my_exception.add_property("message", &regex_exception::get_message);
    Regex_Exception_Type = my_exception.ptr();
    register_exception_translator<regex_exception>(&regex_exception_translator);    
}

void define_module_interface()
{
    boost::python::def("match", match);
}

BOOST_PYTHON_MODULE(cpp_regex)
{
    setup_exceptions();
    define_module_interface();
}

