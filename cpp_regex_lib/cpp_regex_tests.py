import cpp_regex
import unittest


class TestCppRegexMatch(unittest.TestCase):

	def test_simple_regex(self):
		text = 'filename.txt'
		regex = '[a-z]+\\.txt$'
		expected = [text]
		self.assertEqual(expected, cpp_regex.match(text, regex))

	def test_multimatch(self):
		major = str(1)
		minor = str(2)
		text = 'v' + major + '.' + minor
		regex = '^v(\\d).(\\d)$'
		expected = [text, major, minor]
		self.assertEqual(expected, cpp_regex.match(text, regex))

	def test_exceptions(self):
		text = ''
		regex = '\\'
		self.assertRaises(cpp_regex.regex_exception, cpp_regex.match, text, regex)


if __name__ == '__main__':
	unittest.main()
