from django.http import HttpResponse
from django.template import RequestContext, loader
import cpp_regex

def process(text_to_process, regular_expression):
	try:
		return cpp_regex.match(str(text_to_process), str(regular_expression))
	except cpp_regex.regex_exception as exception:
		return ['C++ Regex Exception:', exception.message]

def homepage(request, text_to_process, regular_expression):
	matches = process(text_to_process, regular_expression)

	template = loader.get_template('website/match.html')
	context = RequestContext(request, {
		'title' : 'C++ regex tester',
		'text_to_process' : text_to_process,
		'regular_expression' : regular_expression,
		'matches' : matches
		})
	return HttpResponse(template.render(context))
